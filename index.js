
const express = require(`express`);
const mongoose = require(`mongoose`);
const dotenv = require(`dotenv`);
dotenv.config();

const app = express();
const PORT = 3001;


app.use(express.json())
app.use(express.urlencoded ({extended:true}))


mongoose.connect(process.env.MONGO_URL,{useNewUrlParser: true, useUnifiedTopology: true})

const db = mongoose.connection

db.on("error", console.error.bind(console, `connection error:`))
db.once(`open`, () => console.log(`Connected to Database`))

const userSchema = new mongoose.Schema(
	{
		userName: {
			type: String,
			required: [true, `Username is required`]
		},

		password: {
			type: String,
			required: [true, `Password is required`]
		} 
	}

)

const User = mongoose.model("User", userSchema)


app.post('/signup', (req, res) =>{

	res.send(`New user registered`)

	let newUser = new User({
		userName: req.body.username,
		password: req.body.password
	})
	newUser.save().then((result)=> {
		if(result){
			return res.status(200).send(`User ${req.body.username} is already registered`)
		}else{
			return res.status(500).json(err)
		}
	})

	
})



app.get(`/signup`, (req, res) => {
	User.find().then( (docs, err) =>{
		if(docs){
			return res.status(200).send(docs)
		} else {
			return res.status(500).json(err)
		}
	})
})




app.listen(PORT, () => console.log(`Server connected at port ${PORT}`))